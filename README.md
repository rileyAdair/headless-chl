# Headless CHL

A Headless WordPress site. Built with Nuxt.js.

## Setup

Clone the project to your local machine

```bash
git clone git@gitlab.com:rileyAdair/headless-chl.git
```

Install dependencies

```bash
npm install
```

Request routes and OAuth credentials from owner and add to `config.js` in root directory.

## Run

Serve with hot reload at localhost:3000

```bash
npm run dev
```

## Build

Build for production and launch server

```bash
npm run build
npm start
```

Generate static project

```bash
npm run generate
```

For detailed explanation on how Nuxt.js works, checkout the [docs](https://nuxtjs.org/).



## WordPress Setup

[Headless Click Here Labs](http://headless.clickherelabs.com/wp-admin/plugins.php)

### REST API Plugins

This project uses the following WordPress REST API plugins:

----

[ACF to REST API](https://wordpress.org/plugins/acf-to-rest-api/)

**Required** for Advanced Custom Fields usage.

Exposes Advanced Custom Fields endpoints in the WordPress REST API.

----
[Gravity Forms REST API](https://docs.gravityforms.com/web-api-v2/)

**Required** for Gravity Forms usage.

Requires Basic or OAuth 1.0a Authentication for plugin usage. This project utilizes OAuth 1.0a Authentication with the **WP REST API - OAuth 1.0a Server** plugin. Following this Gravity Forms recommended [tutorial](https://code.tutsplus.com/tutorials/wp-rest-api-setting-up-and-using-oauth-10a-authentication--cms-24797), permanent OAuth credentials for this project were generated with [Postman](https://www.getpostman.com/). Full plugin documentation can be found [here](https://docs.gravityforms.com/web-api-v2/).

An Express [server](/server/index.js) was created to proxy the Gravity Forms REST API calls. This server was modeled after Nuxt.js [example](https://nuxtjs.org/api/nuxt-render/) documentation.

----
[WP REST API - OAuth 1.0a Server](https://wordpress.org/plugins/rest-api-oauth1/)

**Required** for OAuth 1.0a Authentication.

Authenticates site via OAuth 1.0a.

----
[WP REST API Yoast SEO](https://wordpress.org/plugins/wp-api-yoast-meta/)

**Required** for Yoast SEO usage.

Adds Yoast fields to page and post metadata to WordPress REST API responses.

----
[Better REST API Featured Images](https://wordpress.org/plugins/better-rest-api-featured-images/)

**optional**

Adds a top-level field with featured image data including available sizes and URLs to the post object returned by WordPress REST API.

----
[WP-REST-API V2 Menus](https://wordpress.org/plugins/wp-rest-api-v2-menus/)

**optional**

Adds menus endpoints on WordPress REST API.

----

### All Plugins

The following is a list of all WordPress plugins used in this project:

* [ACF to REST API](https://wordpress.org/plugins/acf-to-rest-api/)  
* [Advanced Custom Fields PRO](https://www.advancedcustomfields.com/)  
* [Better REST API Featured Images](https://wordpress.org/plugins/better-rest-api-featured-images/)  
* [Custom Post Type UI](https://wordpress.org/plugins/custom-post-type-ui/)  
* [Gravity Forms](https://www.gravityforms.com/)  
* [Gravity Forms REST API](https://docs.gravityforms.com/web-api-v2/)  
* [WP REST API - OAuth 1.0a Server](https://wordpress.org/plugins/rest-api-oauth1/)  
* [WP REST API Yoast SEO](https://wordpress.org/plugins/wp-api-yoast-meta/)  
* [WP-REST-API V2 Menus](https://wordpress.org/plugins/wp-rest-api-v2-menus/)  
* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/)  
