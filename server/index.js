const { Nuxt, Builder } = require('nuxt');
const { json } = require('body-parser');
const express = require('express');
const app = express();

const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3000;

// Body parser, to access `req.body`
app.use(json());

// Import API Routes
const api = require('./api/index.js');
app.use(api);

// Import and set Nuxt.js options
let config = require('../nuxt.config.js');
config.dev = (process.env.NODE_ENV !== 'production');

// Init Nuxt.js
const nuxt = new Nuxt(config);

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt);
  builder.build();
} else {
  listen();
}

// Give Nuxt middleware to Express
app.use(nuxt.render);

// Listen the server
app.listen(port, host);
console.log(`Server is listening on ${host}:${port}`);