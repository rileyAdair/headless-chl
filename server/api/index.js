const { Router } = require('express');
const router = Router();

const gravityForm = require('./gravityForm');
router.use(gravityForm)

module.exports = router;