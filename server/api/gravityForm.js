const { Router } = require('express');
const router = Router();
const axios = require('axios');
const { routes } = require('../../config');
const authorization = require('./authorization');

router.get('/api/getForm', (req, res, next) => {

  const method = 'GET';
  const route = routes.getForm;
  const authorizationHeader = authorization(method, route);

  axios({
    method: method,
    url: route,
    headers: {
      'Authorization': authorizationHeader
    }
  })
    .then(response => res.json(response.data))
    .catch(err => res.json(err))
});

router.post('/api/postForm', (req, res, next) => {

  const method = 'POST';
  const route = routes.postForm;
  const authorizationHeader = authorization(method, route);

  axios({
    method: method,
    url: route,
    data: req.body,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': authorizationHeader
    }
  })
    .then(response => res.json(response.data))
    .catch(err => res.json(err))
});

module.exports = router;