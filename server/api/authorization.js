const oauthSignature = require('oauth-signature');
const { oauth } = require('../../config.js');

const authorization = (method, route) => {
  const parameters = {
    oauth_consumer_key : oauth.consumerKey, 
    oauth_token : oauth.accessToken, 
    oauth_signature_method : 'HMAC-SHA1',
    oauth_timestamp : '',
    oauth_nonce : '',
    oauth_version : '1.0'
  };

  const consumerSecret = oauth.consumerSecret;
  const tokenSecret = oauth.tokenSecret;

  const generateNonce = () => {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 11; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  const requestParams = { ...parameters };
  requestParams.oauth_nonce = generateNonce(); // unique identifier
  requestParams.oauth_timestamp = new Date()
    .getTime()
    .toString()
    .slice(0,10); // first 10 digits from current time

  const encodedSignature = oauthSignature.generate(
    method,
    route,
    requestParams,
    consumerSecret,
    tokenSecret
  )

  const authorizationHeader = `OAuth oauth_consumer_key="${requestParams.oauth_consumer_key}", oauth_token="${requestParams.oauth_token}", oauth_signature_method="${requestParams.oauth_signature_method}", oauth_timestamp="${requestParams.oauth_timestamp}", oauth_nonce="${requestParams.oauth_nonce}", oauth_version="${requestParams.oauth_version}", oauth_signature="${encodedSignature}"`;

  return authorizationHeader;
}

module.exports = authorization;