module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Click Here Labs',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'a headless wordpress proof of concept' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Share variables & mixins across all SASS styles
  */
  modules: [
    ['nuxt-sass-resources-loader', [
      '@/assets/scss/common/mixins.scss',
      '@/assets/scss/common/variables.scss',
    ]],
  ],
}
